import visidia.simulation.process.algorithm.Algorithm;
import visidia.simulation.process.messages.StringMessage;

public class Test extends Algorithm {

    @Override
    public Object clone() {
        return new Test();
    }

    @Override
    public void init() {
        System.out.println("hi " + getId());

        if(getId() == 0) {

            sendTo(0, new StringMessage("how are you"));
            sendTo(1, new StringMessage("hhhhhhhhh"));
        } else if(getId() == 1) {

            String msg = receiveFrom(0).toString();
            System.out.println("msg is : " + msg);
        } else if(getId() == 2) {
            String msg = receiveFrom(1).toString();
            System.out.println("msg is : " + msg);
        } else {
            System.out.println("nothing");
        }
    }
}
