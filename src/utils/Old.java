package utils;

import visidia.simulation.process.messages.StringMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azz_b on 03/01/17.
 */
public class Old {

    /*import visidia.simulation.process.algorithm.Algorithm;
import visidia.simulation.process.messages.Door;
import visidia.simulation.process.messages.StringMessage;

import java.util.ArrayList;
import java.util.List;

public class MainAlgo extends Algorithm {

    @Override
    public Object clone() {
        return new MainAlgo();
    }

    @Override
    public void init() {
        Process p;
        //Receiver receiver1;
        //Receiver receiver2;

        if (getId() == 0) {
            p = new Process(this, getId());
            p.snapshotLocalState();
            sleep();
            sendMessage(-1, "other");
            sleep();
            sendMessage(-1, "other again");
        } else if (getId() == 1) {

            p = new Process(this, getId());

        } else if (getId() == 2) {

            p = new Process(this, getId());

        } else {
            System.out.println("nothing process");
            p = new Process(this, 0);
        }

        if(receiveFrom(0).toString().equals(Process.MARKER)) {
            p.snapshotLocalState();
        }

        if(getId() != 0) {
           // receiver1 = startReceiverAtPort(0, p);
            //receiver2 = startReceiverAtPort(1, p);
            while(p.localState == State.READY) {
                String msg = receiveFrom(0).toString();
                System.out.println("msg received : " + msg);
                p.addToCin(msg);
            }
        }
    }

    /**
     *  @param  port  port number which we want to receive from it
     *  @param  p   Process instance that hold the node
     * */
/*/*
    public MainAlgo.Receiver startReceiverAtPort(int port, MainAlgo.Process p) {
        MainAlgo.Receiver receiver = new MainAlgo.Receiver(port).setContext(this).setProcess(p);
        receiver.start();
        return receiver;
    }

    /**
     * @param port  of process that we will send to it if it's different of -1
     * */
/*
    public void sendMessage(int port, String msg) {
        if(port != -1) {
            sendTo(port, new StringMessage(msg));
        } else {
            sendAll(new StringMessage(msg));
        }
    }

    public void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     *  class Process with all proprieties needs for CHANDY LAMPORT Algorithm
     * */
/*
    public class Process {
        public static final int SIZE = 100;
        public static final String MARKER = "MARKER";
        public MainAlgo context;

        private int id;
        private MainAlgo.State localState;       // état local
        private boolean hasRegistedState;
        private String[] c_in;         // cannal d'entrer
        private String[] c_out;        // cannal de sortie
        private boolean[] receipt;     // réçus
        private List<String> queue;

        // constructor

        public Process(MainAlgo context , int id) {

            if(context != null) this.context = context;

            this.id = id;
            localState = MainAlgo.State.NONE;
            hasRegistedState = false;
            c_in    = new String[SIZE];
            c_out   = new String[SIZE];
            receipt = new boolean[SIZE];
            initializeReceipt();
            queue   = new ArrayList<>();

        }

        public void snapshotLocalState() {
            if(!hasRegistedState) {
                localState = MainAlgo.State.READY;
                hasRegistedState = true;
                context.sendMessage(-1, MARKER);
                showInfo();
                return;
            }
            System.out.println("the process with id: {" + id + "} has already state");
        }

        /**
         * add the message received from another process to "c_in"
         * */

  /*      public synchronized void addToCin(String msg) {
            if(msg.equals(MARKER)) {
                this.snapshotLocalState();
            } else {
                queue.add(msg);
                int index = getIndexOfFreeCase();
                if(index != -1) {
                    c_in[index] = msg;
                    receipt[index] = true;
                }
            }
        }

        /**
         * initialize array for all case with "false"
         * */

     /*   public void initializeReceipt() {
            for(int i = 0; i < SIZE; i++) {
                receipt[i] = true;
            }
        }

        /**
         * get index of array receipt that has free case when it's value is "false"
         * return -1 when no free case in array
         * */
       /* public int getIndexOfFreeCase() {
            int i = 0;
            while(i < SIZE  && receipt[i] ) i++;
            if(i == SIZE) return -1;
            return i;
        }

        public void showInfo() {
            System.out.println("********************* BEGIN INFO OF PROCESS {" + id + "}********************************");
            System.out.println("{\n  LocalState: " + localState + "\n  HasRegistredState: " + hasRegistedState + " \n}");
            System.out.println("\n\n");
        }
    }

    /**
     * Enum State for local state "état local d'un processus"
     */

    /*public enum State {
        NONE,
        READY,
    }


    /**
     *  Receive Thread
     * */

    /*class Receiver extends Thread{

        private MainAlgo context;
        private MainAlgo.Process process;
        private int port;

        public Receiver(int port) {
            this.port = port;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(process.localState == MainAlgo.State.READY) {
                String msg = context.receiveFrom(port).toString();
                System.out.println("msg received at thread p{" + process.id + "}: " + msg);
                process.addToCin(msg);
            }

            System.out.println("the process changed his local state");
        }

        public MainAlgo.Receiver setContext(MainAlgo context) {
            this.context = context;
            return this;
        }

        public MainAlgo.Receiver setProcess(MainAlgo.Process process) {
            this.process = process;
            return this;
        }
    }


}*/
}
